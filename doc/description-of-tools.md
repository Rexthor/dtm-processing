# A brief description of software and tools for DTM analysis


## GDALDEM

- Geospatial Data Abstraction Library
- Website: <https://gdal.org/>
- Written in C / C++ / Python
- License: MIT
- [`gdaldem`](https://gdal.org/programs/gdaldem.html) provides tools to analyze and visualize DEMs
- Usage:
    - CLI via `gdaldem`
    - Numerous bindings and wrappers, e.g. in Python (via osgeo), R (most notably {sf} and {stars}) and Julia (GDAL.jl).


## SAGA GIS

- System for Automated Geoscientific Analyses
- Website: <https://saga-gis.org>
- Written in C++
- License: GNU GPL
- Provides lots of different [tools](https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/index.html) for various tasks.
- We mainly use the tools for terrain analysis here:
    - [Hydrology](https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_hydrology.html)
    - [Morphometry](https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_morphometry.html)
    - [Lighting](https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_lighting.html)
- Note: wxGTK is required in order to build/run SAGA, even when building without GUI (i.e. `./configure --disable-gui`).
- Usage:
    - CLI via `saga_cmd`
    - R: {RSAGA}


## GRASS GIS

- Geographic Resources Analysis Support System 
- Website: <https://grass.osgeo.org/>
- Wiki: <https://grasswiki.osgeo.org/wiki/GRASS-Wiki>
- Reference Manual: <https://grass.osgeo.org/grass80/manuals/>
- Written in C / C++ / Python
- License: GNU GPL
- Usage:
    - CLI via `grass`
    - Python: GRASS Python Scripting Library, PyGRASS
    - R: {rgrass7}
- Note: Installation of `grass-core` is sufficient for core CLI functionality.


## Whitebox Tools

- Whitebox Geospatial Analysis Tools
- Website: <https://www.whiteboxgeo.com>
- GitHub: <https://github.com/jblindsay/whitebox-tools>
- Manual: <https://www.whiteboxgeo.com/manual/wbt_book/intro.html>
- Written in Rust
- License: MIT

### Setup

#### Building from source
- Install Rust / RustUp
  ```sh
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
  ```
- Install Whitebox
  ```sh
  cd /opt/whitebox
  git clone https://github.com/jblindsay/whitebox-tools.git
  cd whitebox-tools
  cargo build --release
  ```
- Docker image
  ```sh
  git clone https://github.com/jblindsay/whitebox-tools.git
  docker build -t whitebox-tools -f docker/whitebox-tools.dockerfile .
  ```

#### R
 
```R
install.packages("whitebox", repos="http://R-Forge.R-project.org")
whitebox::wbt_init()
```

