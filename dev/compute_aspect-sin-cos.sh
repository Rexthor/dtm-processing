#!/usr/bin/zsh

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Prepare Aspect for Aggregation
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Aspect is a circular varible and thus standard aggregation functions 
# lead to misleading results. 
# The aggregation of aspect is therefore based on its sine and cosine.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# source definition of target file / aoi region
source cfg/aoi_config.sh

# input file
aspect_file="dat/output/dtm_orig/${target_file}_aspect.tif"

# output files
out_cos="dat/output/dtm_orig/${target_file}_aspect-cos.tif"
out_sin="dat/output/dtm_orig/${target_file}_aspect-sin.tif"

# check
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: working on '${aspect_file}'"
if [ ! -f $aspect_file ]; then
    echo "File not found. Please check '${aspect_file}.tif'." >&2
    exit 1
fi

echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute cos(aspect)"
gdal_calc.py -A $aspect_file --calc "cos(radians(A))" \
  --format "GTiff" --outfile $out_cos --quiet 

echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute sin(aspect)"
gdal_calc.py -A $aspect_file --calc "sin(radians(A))" \
  --format "GTiff" --outfile $out_sin --quiet

echo "`date "+%Y-%m-%d %H:%M:%S"`: Done"
