#!/usr/bin/zsh

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Prepare Aspect for Aggregation
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Aspect is a circular varible and thus standard aggregation functions 
# lead to misleading results. 
# The aggregation of aspect is therefore based on its sine and cosine.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# source definition of target file / aoi region
source cfg/aoi_config.sh

# input files
asp_cos="dat/output/dtm_aggregates/dtm_spartacus//${target_file}_aspect-cos_sum.tif"
asp_sin="dat/output/dtm_aggregates/dtm_spartacus//${target_file}_aspect-sin_sum.tif"

# output files
asp_atan2="dat/output/dtm_aggregates/dtm_spartacus//${target_file}_aspect-arctan2.tif"

# check input
if [ ! -f $asp_sin ]; then
    echo "File not found. Please check '${asp_sin}.tif'." >&2
    exit 1
fi
if [ ! -f $asp_cos ]; then
    echo "File not found. Please check '${asp_cos}.tif'." >&2
    exit 1
fi

echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Computing arctan2(cos,sin) for aspect of '${target_file}'"
gdal_calc.py -C $asp_cos -S $asp_sin --calc "degrees(arctan2(C,S))" \
  --format "GTiff" --outfile $asp_atan2 --quiet

echo "`date "+%Y-%m-%d %H:%M:%S"`: Done"
