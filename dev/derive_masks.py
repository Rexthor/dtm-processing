#!/usr/bin/env python
"""Calculate masks for dtm and climate data."""

from datetime import datetime
import numpy as np
import rasterio
import subprocess
import xarray as xr
import rioxarray

# file paths
spartacus = "/spartacus/v2.1/02_nc/01_day/RR/RR2022.nc"
dem = "dat/output/dtm_10m/dtm_austria.tif"

spartacus_mask = "dat/output/spartacus_mask.tif"
dem_mask = "dat/output/dtm_10m/dtm_austria_mask.tif"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# dtm coverage: create simple binary raster from dtm
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

with rasterio.open(dem) as src:
    print(f"{datetime.now().strftime('%H:%M:%S')} » Reading input GeoTIFF")
    dtm = src.read(1)

    print(f"{datetime.now().strftime('%H:%M:%S')} » Simplifyig array")

    # set dtm values to 1
    dtm[dtm > 0] = 1

    # set na values to zero
    dtm[dtm == -9999] = 0

    # set data type to 8 bit integer
    out = dtm.astype(np.int8)

    # set gdal metadata
    out_meta = src.meta.copy()
    out_meta.update(
        {
            "driver": "GTiff",
            "width": src.shape[1],
            "height": src.shape[0],
            "count": 1,
            "dtype": "int8",
            "crs": src.crs,
            "transform": src.transform,
            "nodata": 0,
        }
    )

    print(f"{datetime.now().strftime('%H:%M:%S')} » Writing output GeoTIFF")
    with rasterio.open(dem_mask, mode="w", compress="lzw", **out_meta) as dst:
        dst.write(out, 1)

# gdal_translate -ot Byte -a_nodata 0 -co COMPRESS="LZW" \
#     dat/output/dtm_10m/dtm_austria.tif dat/output/dtm_10m/dtm_austria_mask.tif

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# spartacus coverage: create simple binary raster from spartacus data set
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

sparta = xr.open_dataset(spartacus)
# sparta.rio.write_crs("epsg:3416", inplace=True)
sparta.mask.rio.to_raster(spartacus_mask)

call = f"gdal_edit.py -a_srs EPSG:3416 {spartacus_mask}"
subprocess.call(call, shell=True)
