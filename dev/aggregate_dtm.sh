#!/usr/bin/zsh

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Aggregate DTM to SPARTACUS grid
# https://gdal.org/programs/gdalwarp.html
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# gdalinfo dat/output/spartacus_mask.tif

# Note: rms requires (GDAL >= 3.3)
currentver="$(gdalinfo --version | grep -Po '(?<=GDAL )[^,]+')"
requiredver="3.3.0"
if [ "$(printf '%s\n' "$requiredver" "$currentver" | sort -V | head -n1)" = "$requiredver" ]; then
    print -P '%B%F{green}\u2714 GDAL Version: ${currentver} >= ${requiredver} %f%b'
else
    print -P '%B%F{red}\u2717 WARNING: GDAL >= ${requiredver} is required to compute RMS aggregates.
           The installed version is ${currentver}.%f%b'
fi

resampling_method="max min q1 q3 average med rms"

outpath="dat/output/dtm_aggregates/dtm_spartacus"

all_funs="$resampling_method mode sum"
for fun in ${(z)all_funs}; do
    mkdir ${outpath}/${fun}
done

for infile in dat/output/dtm_orig/**/*(.); do

    if [[ $infile =~ "mask" ]]; then
        continue
    fi
    
    if [[ $infile =~ "sinks-filled" ]]; then
        continue
    fi

    if [[ $infile =~ "geomorphons" ]]; then
        aggfun="mode"
        outfile_base=${${infile:t}//.tif/_${aggfun}.tif}
        outfile="${outpath}/${aggfun}/${outfile_base}"
        if [[ -f "$outfile" ]]; then
            echo "`date "+%Y-%m-%d %H:%M:%S"`: Skipping - *$outfile:t* computed already\n"
        else
            echo "`date "+%Y-%m-%d %H:%M:%S"`: Using resampling method 'mode' for *$infile:t*\n"
            gdalwarp -tr 1000 1000 -r mode -te 112000 258000 696000 587000 $infile $outfile
        fi
        echo "-----------------------------------------------------------"
    elif [[ $infile =~ "aspect" ]]; then
        if [[ $infile =~ "cos|sin" ]]; then
            aggfun="sum"
            outfile_base=${${infile:t}//.tif/_${aggfun}.tif}
            outfile="${outpath}/${aggfun}/${outfile_base}"
            if [[ -f "$outfile" ]]; then
                echo "`date "+%Y-%m-%d %H:%M:%S"`: Skipping - *$outfile:t* computed already\n"
            else
                echo "`date "+%Y-%m-%d %H:%M:%S"`: Using resampling method 'sum' for *$infile:t*\n"
                gdalwarp -tr 1000 1000 -r sum -te 112000 258000 696000 587000 $infile $outfile
            fi
            echo "-----------------------------------------------------------"
        fi
    else
        for aggfun in ${(z)resampling_method}; do
            outfile_base=${${infile:t}//.tif/_${aggfun}.tif}
            outfile="${outpath}/${aggfun}/${outfile_base}"
            if [[ -f "$outfile" ]]; then
                echo "`date "+%Y-%m-%d %H:%M:%S"`: Skipping - *$outfile:t* computed already\n"
            else
                echo "`date "+%Y-%m-%d %H:%M:%S"`: Using resampling method '$aggfun' for *$infile:t*\n"
                gdalwarp -tr 1000 1000 -r $aggfun -te 112000 258000 696000 587000 $infile $outfile
            fi
            echo "-----------------------------------------------------------"
        done
    fi
done
