#!/usr/bin/zsh

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# SAGA-based terrain analysis
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# SAGA Tool Library Overview
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/index.html
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Terrain Analysis Libraries
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_hydrology.html
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_morphometry.html
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_slope_stability.html
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_lighting.html
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# source definition of target file / aoi region
source cfg/aoi_config.sh

# check
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: working on '${target_file}'"
if [ ! -f dat/interim/dtm/${target_file}.tif ]; then
    echo "File not found. Please check 'dat/interim/dtm/${target_file}.tif'." >&2
    exit 1
fi

# create saga raster data set
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Convert GeoTIFF to SAGA GIS Binary Grid File Format"
gdal_translate -of SAGA dat/interim/dtm/${target_file}.tif \
  dat/interim/sagadata/${target_file}.sdat

gdal_translate -of SAGA dat/interim/whiteboxdata/${hydro_file}.tif \
  dat/interim/sagadata/${hydro_file}.sdat

# setup file paths
dtm_elev="dat/interim/sagadata/${target_file}.sdat"
dtm_hydrocond="dat/interim/sagadata/${hydro_file}.sdat"

outpath_elevbase="dat/interim/sagadata/${target_file}"
outpath_hydrobase="dat/interim/sagadata/${hydro_file}"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# land-surface variables - preprocessing
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# Slope and Aspect (RAD)
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_morphometry_0.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Slope and Aspect (RAD)"
saga_cmd ta_morphometry 0 -ELEVATION $dtm_elev \
    -SLOPE "${outpath_elevbase}_slope-rad.sdat" \
    -ASPECT "${outpath_elevbase}_aspect-rad.sdat"

# Fill Sinks (Wang & Liu)
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_preprocessor_4.html
# echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Fill Sinks"
# saga_cmd ta_preprocessor 4 -ELEV $dtm_elev \
#     -FILLED "${outpath_elevbase}_sinks-filled.sdat"

# Fill Sinks (Planchon/Darboux, 2001)
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_preprocessor_7.html
# echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Fill Sinks"
# saga_cmd ta_preprocessor 3 -DEM $dtm_elev \
#     -RESULT "${outpath_elevbase}_sinks-filled.sdat"

# Breach Depressions
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_preprocessor_7.html
# echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Fill Sinks"
# saga_cmd ta_preprocessor 7 -DEM $dtm_elev \
#     -NOSINKS "${outpath_elevbase}_breach-depressions.sdat"

# setup file paths
dtm_slope="${outpath_elevbase}_slope-rad.sdat"
# dtm_nosinks="${outpath_elevbase}_sinks-filled.sdat"
# use dtm_hydrocond=$dtm_nosinks when using a sink filled dtm computed via SAGA

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# land-surface variables - hydrology
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# Flow Accumulation / Catchment Area & Accumulated Material
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_hydrology_0.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Flow Accumulation"
saga_cmd ta_hydrology 0 -ELEVATION $dtm_hydrocond \
    -FLOW "${outpath_hydrobase}_flow-accumulation.sdat"

# Melton Ruggedness Number
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_hydrology_23.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Melton Ruggedness Number"
saga_cmd ta_hydrology 23 -DEM $dtm_elev \
    -AREA "${outpath_elevbase}_catchment-area.sdat" \
    -ZMAX "${outpath_elevbase}_maximum-height.sdat" \
    -MRN "${outpath_elevbase}_MRN.sdat"

# Flow Path Length
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_hydrology_6.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Flow Path Length"
saga_cmd ta_hydrology 6 -ELEVATION $dtm_hydrocond \
    -LENGTH "${outpath_hydrobase}_flow-path-length.sdat"

# Flow Width and Specific Catchment Area
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_hydrology_19.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Flow Path Width / SCA"
saga_cmd ta_hydrology 19 -DEM $dtm_hydrocond -TCA "${outpath_hydrobase}_catchment-area.sdat" \
    -WIDTH "${outpath_hydrobase}_flow-width.sdat" \
    -SCA "${outpath_hydrobase}_SCA.sdat"

# Stream Power Index
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_hydrology_21.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Stream Power Index"
saga_cmd ta_hydrology 21 -SLOPE $dtm_slope -AREA "${outpath_elevbase}_SCA.sdat" \
    -SPI "${outpath_elevbase}_SPI.sdat"

# SAGA Wetness Index
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_hydrology_15.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Topographic Wetness Index"
saga_cmd ta_hydrology 15 -DEM $dtm_hydrocond \
    -AREA_MOD "${outpath_hydrobase}_mod-catchment-area.sdat" \
    -TWI "${outpath_hydrobase}_TWI.sdat"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# land-surface variables - watershed delineation
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# Channel Network
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_channels_0.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Channel Network"
saga_cmd ta_channels 0 -ELEVATION $dtm_hydrocond \
    -INIT_GRID "${outpath_hydrobase}_flow-accumulation.sdat" \
    -CHNLNTWRK "${outpath_hydrobase}_channel-network.sdat" \
    -INIT_VALUE 1000 \
    -INIT_METHOD "2"

# Watershed Basins
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_channels_0.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Watershed Basins"
saga_cmd ta_channels 1 -ELEVATION $dtm_hydrocond \
    -CHANNELS "${outpath_hydrobase}_channel-network.sdat" \
    -BASINS "${outpath_hydrobase}_watershed-basins.sdat"

# Vectorize Grid Classes
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/shapes_grid_6.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Vectorize Basin Grids"
saga_cmd shapes_grid 6 -GRID "${outpath_hydrobase}_watershed-basins.sdat" \
    -POLYGONS "${outpath_hydrobase}_watershed-basins.gpkg"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# land-surface variables - morphometry
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# Curvature
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_morphometry_0.html
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_morphometry_23.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Curvature"
saga_cmd ta_morphometry 23 -DEM $dtm_elev \
    -MINIC "${outpath_elevbase}_curv-min.sdat" \
    -MAXIC "${outpath_elevbase}_curv-max.sdat" \
    -PLANC "${outpath_elevbase}_curv-plan.sdat" \
    -PROFC "${outpath_elevbase}_curv-prof.sdat"

# Normalized Height
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_morphometry_14.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Normalized Height"
saga_cmd ta_morphometry 14 -DEM $dtm_elev \
    -NH "${outpath_elevbase}_NH.sdat" -W=5 -T=2 -E=2

# Convergence Index
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_morphometry_1.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Convergence Index"
saga_cmd ta_morphometry 1 -ELEVATION $dtm_elev \
    -RESULT "${outpath_elevbase}_convergence-index.sdat"

# Vector Ruggedness Measure
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_morphometry_17.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Vector Ruggedness Measure"
saga_cmd ta_morphometry 17 -DEM $dtm_elev \
    -VRM "${outpath_elevbase}_VRM.sdat"

# Terrain Surface Convexity
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_morphometry_21.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Terrain Surface Convexity"
saga_cmd ta_morphometry 21 -DEM $dtm_elev \
    -CONVEXITY "${outpath_elevbase}_convexity.sdat"

# Diurnal Anisotropic Heat
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_morphometry_12.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Diurnal Anisotropic Heat"
saga_cmd ta_morphometry 12 -DEM $dtm_elev \
    -DAH "${outpath_elevbase}_DAH.sdat"

# Wind Exposition Index
# https://saga-gis.sourceforge.io/saga_tool_doc/7.1.1/ta_morphometry_27.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Wind Exposition Index"
saga_cmd ta_morphometry 27 -DEM $dtm_elev \
    -EXPOSITION "${outpath_elevbase}_WEI.sdat"

# Terrain surface classification based on terrain surface convexity
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_morphometry_22.html
# Landform classification based on the profile and tangential (across slope) curvatures
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_morphometry_4.html
# Fuzzy landform element classification
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_morphometry_25.html

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# land-surface variables - lighting
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# Geomorphons
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_lighting_8.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Geomorphons"
saga_cmd ta_lighting 8 -DEM $dtm_elev \
    -GEOMORPHONS "${outpath_elevbase}_geomorphons.sdat"

# Sky View Factor
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_lighting_3.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute SVF"
saga_cmd ta_lighting 3 -DEM $dtm_elev \
    -SVF "${outpath_elevbase}_SVF.sdat"

# Topographic Openness
# https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_lighting_5.html
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Compute Topographic Openness"
saga_cmd ta_lighting 5 -DEM $dtm_elev \
    -POS "${outpath_elevbase}_PTO.sdat" \
    -NEG "${outpath_elevbase}_NTO.sdat"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# prepare final output
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# convert saga raster data sets back to geotiff
echo "\n`date "+%Y-%m-%d %H:%M:%S"`: Convert SAGA GIS Binary Grid File Format back to GeoTIFF"
cd dat/interim/sagadata
for s in *.sdat; do 
  echo "------------------------------------------------------------"
  t=$(echo $s | sed 's/\.sdat$/.tif/')
  if [[ "$s" == "${target_file}.sdat" ]]; then
      continue
  fi
  gdal_translate -of GTiff $s ../../output/dtm_orig/$t
  echo "converted '$s' >> '$t'"
done
cd ....

echo "`date "+%Y-%m-%d %H:%M:%S"`: Done"
