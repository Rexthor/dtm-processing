#!/usr/bin/zsh

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Prepare DTM Dataset
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#
# Dataset: HydroSHEDS DTM v1
#   - Data Basis: SRTM + River / Water Bodies Data
#   - CRS: WGS84 (EPSG:4326)
#   - Metadata: https://www.hydrosheds.org/
#
# Reprojection:
#   - Using a Geographic Coordinate System would result in erroneous results in many DTM algorithms
#   - The HydroSHEDS DEM is available in WGS84 (EPSG:4326).
#     Reprojection to ERTS89 / Austria Lambert is performed for
#     the sake of consistency with ZAMG's gridded climate data for Austria.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# Downlaod and unzip GeoTIFF
wget https://data.hydrosheds.org/file/hydrosheds-v1-con/eu_con_3s.zip -P dat/raw/dtm/
wget https://data.hydrosheds.org/file/hydrosheds-v1-acc/eu_acc_3s.zip -P dat/raw/dtm/
unzip dat/raw/dtm/eu_con_3s.zip -d dat/raw/dtm
unzip dat/raw/dtm/eu_acc_3s.zip -d dat/raw/dtm

# Reproject to common coordinate reference system
# t_srs=$(cat cfg/wkt_3416.prj | egrep -v "(^#.*|^$)")
gdalwarp -s_srs EPSG:4326 -t_srs EPSG:3416 -dstnodata "-9999" \
    -cutline dat/raw/aoi/austria_3416.gpkg -crop_to_cutline -tr 77 77 \
    dat/raw/dtm/eu_con_3s.tif dat/interim/dtm/hydrosheds_austria.tif

gdalwarp -s_srs EPSG:4326 -t_srs EPSG:3416 -dstnodata 0 \
    -cutline dat/raw/aoi/austria_3416.gpkg -crop_to_cutline -tr 77 77 \
    dat/raw/dtm/eu_acc_3s.tif dat/interim/dtm/hydrosheds_acc_austria.tif
