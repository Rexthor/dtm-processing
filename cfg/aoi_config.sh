#!/usr/bin/zsh

# Basenames of files to be processed
target_file="dtm_austria"
hydro_file="dtm_austria_depressions_breached"

# Filename extension
fn_extension=".tif"

# GRASS GIS location (subdirectory)
gis_dir="dtm_location"
