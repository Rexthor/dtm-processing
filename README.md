# DTM Processing

Derivation of DTM features, specifically with respect to geomorphological and hydrological characteristics.

## Scripts

1. `get_at_dtm.sh`: Download DTM OGD data and reproject to ETRS89 / Austria Lambert.
1. `get_hydrosheds.sh`: Download HydroSHEDS data and reproject to ETRS89 / Austria Lambert.
1. `fill_depressions.R`: Perform depression breaching and filling to prepare the DTM for hydrological analysis[^1],[^2].
1. `compute_gdaldem.sh`: Compute standard [`gdaldem`](https://gdal.org/programs/gdaldem.html) features.
1. `compute_whitebox.R`: Compute various features for terrain analysis using [WhiteboxTools Open Core](https://www.whiteboxgeo.com/geospatial-software/).
1. `compute_saga.sh`: Compute various features for terrain analysis using SAGA GIS[^3].
1. `compute_aspect-sin-cos.sh`: Compute average aspect pt.1: sin(aspect) and cos(aspect).
1. `aggregate_dtm.sh`: Aggregate DTM and all derivatives to the SPARTACUS[^4],[^5] grid.
1. `compute_aspect-arctan2.sh`: Compute average aspect pt.2: arctan2(cos(aspect),sin(aspect)).
1. `derive_masks.py`: Derive (binary) masks for original DTM and target grid.  

*Note*: GRASS GIS processing routines are currently not yet implemented.

1. `setup_grass.sh`: Setup of GRASS GIS database
1. `compute_grass.py`: Some chunks for calling GRASS GIS through the GRASS Python Scripting Library.

## Repo Structure

The repo is structured as follows:
- `cfg`: configuration files
- `dat`: data sets
- `dev`: development (scripts)
- `doc`: documentation
- `gis`: GIS projects
- `org`: organizational stuff
- `plt`: plots / figures

Specifically, the data folder `dat` is structured as follows:

```sh
dat
├── interim            » Intermediate data that has been transformed.
│   ├── dtm            » 
│   ├── grassdata      » Temporary GRASS data (GRASS Raster Format).
│   ├── sagadata       » Temporary SAGA data (SAGA GIS Binary Grid File Format).
│   └── whiteboxdata   » Temporary WhiteBox data.
├── output             » Canonical output data sets.
│   ├── dtm_aggregates » Aggregated versions of the DTM.
│   └── dtm_orig       » Results on the original DTM grid.
└── raw                » The original, immutable data dump.
    ├── aoi            » Vector data specifying the area of interest.
    └── dtm            » Original digital terrain model data.
```

# References
[^1]: Lindsay J.B. (2016): Efficient hybrid breaching-filling sink removal methods for flow path enforcement in digital elevation models. *Hydrological Processes*, 30(6): 846–857. [doi:10.1002/hyp.10648](https://doi.org/10.1002/hyp.10648).

[^2]: Lindsay J.B. & Dhun K (2015): Modelling surface drainage patterns in altered landscapes using LiDAR. *International Journal of Geographical Information Science*, 29: 1-15. [doi:10.1080/13658816.2014.975715](https://doi.org/10.1080/13658816.2014.975715).

[^3]: For details see the [SAGA-GIS Tool Library Documentation](https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/index.html), specifically the Terrain Analysis Toolboxes for [Hydrology](https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_hydrology.html), [Morphometry](https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_morphometry.html) and [Lighting](https://saga-gis.sourceforge.io/saga_tool_doc/8.0.0/ta_lighting.html).

[^4]: Hiebl J. & Frei C. (2016): Daily temperature grids for Austria since 1961—concept, creation and applicability. *Theoretical and Applied Climatology* 124, 161–178, [doi:10.1007/s00704-015-1411-4](https://doi.org/10.1007/s00704-015-1411-4).

[^5]: Hiebl J. & Frei C. (2018): Daily precipitation grids for Austria since 1961—development and evaluation of a spatial dataset for hydro-climatic monitoring and modelling. *Theoretical and Applied Climatology* 132, 327–345, [doi:10.1007/s00704-017-2093-x](https://doi.org/10.1007/s00704-017-2093-x).
